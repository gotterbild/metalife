<?get_header()?>

	<main class="inner">
		<div class="grid">

			<? if( have_posts() ): ?>
					<? while( have_posts() ) : the_post(); ?>

						<article>

							<img
								class="thumb"
								src="
									<?
									if( has_post_thumbnail($post->post_id) ):
										ml_post_thumb();
									else:
										echo get_template_directory_uri()."/images/no-image.png";
									endif;
									?>"
								alt="<?=$post->post_title?>"
							>

							<h1><?=$post->post_title?></h1>

							<? the_content(false); ?>

						</article>

					<?endwhile;?>
				<?endif;?>

			<div class="mainh1">
				<p>Метафизика жизни - Создаём Будущее!</p>
			</div>

		</div>
	</main>

<?get_footer()?>