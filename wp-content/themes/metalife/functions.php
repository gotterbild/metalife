<?

//Ставим в очередь на загрузку стили и скрипты
if( !preg_match("/wp-admin/", $_SERVER['PHP_SELF']) ):

	wp_enqueue_script( 'metalife-script', get_template_directory_uri().'/js/script.js', array(), null, true );

endif;

//Чтобы постам можно было задавать картинки
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support('post-thumbnails');

	function ml_post_thumb() {
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'full', true);
		echo $thumb_url[0];
	};

};

//Регистрируем сайдбары
if ( function_exists('register_sidebar') ):

	register_sidebar(array(
		'name' => __( 'После постов', 'metalife' ),
		'id' => 'afterpost-sidebar',
		'before_widget' => '<article class="afterpost-widget widget %2$s">',
		'after_widget' => '<div class="clr"></div></div></article>',
		'before_title' => '<h2>',
		'after_title' => '</h2><div class="main">',
	));

endif;


//Регистрируем меню
register_nav_menus(array(
	'top'    => 'Верхнее меню',    //Название месторасположения меню в шаблоне
));

//Свой создатель меню, чтобы не было li и классы подставлялись ссылкам
class Svoboda_Walker extends Walker_Nav_Menu
{
    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array|object $args    Additional strings. Actually always an
                                     instance of stdClass. But this is WordPress.
     * @return void
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )
    {
        $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;

        $class_names = join(
            ' '
        ,   apply_filters(
                'nav_menu_css_class'
            ,   array_filter( $classes ), $item
            )
        );

        ! empty ( $class_names )
            and $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= "";

        $attributes  = '';

        ! empty( $item->attr_title )
            and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
        ! empty( $item->target )
            and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
        ! empty( $item->xfn )
            and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
        ! empty( $item->url )
            and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';

        // insert description for top level elements only
        // you may change this
        $description = ( ! empty ( $item->description ) and 0 == $depth )
            ? '<small class="nav_desc">' . esc_attr( $item->description ) . '</small>' : '';

        $title = apply_filters( 'the_title', $item->title, $item->ID );

        $item_output = $args->before
            . "<a $class_names $attributes>"
            . $args->link_before
            . $title
            . '</a> '
            . $args->link_after
            . $description
            . $args->after;

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el'
        ,   $item_output
        ,   $item
        ,   $depth
        ,   $args
        );
    }
}

// Removes li class from wp_nav_menu
function remove_li ( $menu ){
    return preg_replace( '#</li>#', '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_li' );

//Пагинация
function wp_corenavi() {
  global $wp_query;
  $pages = '';
  $max = $wp_query->max_num_pages;
  if (!$current = get_query_var('paged')) $current = 1;
  $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
  $a['total'] = $max;
  $a['current'] = $current;

  $total = 0; //1 - выводить текст "Страница N из N", 0 - не выводить
  $a['mid_size'] = 2; //сколько ссылок показывать слева и справа от текущей
  $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
  $a['prev_text'] = '<'; //текст ссылки "Предыдущая страница"
  $a['next_text'] = '>'; //текст ссылки "Следующая страница"

  if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>'."\r\n";
  echo $pages . paginate_links($a);
}


//Генерация своего кода комметариев
class Svoboda_Comment_Walker extends Walker_Comment {
	var $tree_type = 'comment';
	var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );

	// start_el – HTML for comment template
	function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment;
	//	$parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' );

		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'div';
			$add_below = 'comment';
		} ?>

		<div <?php comment_class(empty( $args['has_children'] ) ? '' :'parent') ?> id="comment-<?php comment_ID() ?>">

			<div class="header" role="complementary">
				<a href="#comment_<?php comment_ID() ?>">#<?php comment_ID() ?></a>
				<h4><?php comment_author(); ?></h4>
				<?php if ($comment->comment_approved == '0') : ?>
					<p class="comment-meta-item">Ваш комментарий появится после модерации</p>
				<?php endif; ?>
			</div>

			<div class="text">
				<div class="body">
					<?php comment_text() ?>
				</div>
				<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				<div class="date"><?php comment_date('d F Y') ?>, <?php comment_time() ?></div>
				<div class="clr"></div>
			</div>

		</div>

	<? }

	// end_el – closing HTML for comment template
	function end_el(&$output, $comment, $depth = 0, $args = array() ) {}

};


/* Удаляем стандартные размеры изображений, чтобы вордпресс не создавал доп картинки */
add_filter( 'intermediate_image_sizes', 'delete_intermediate_image_sizes' );
function delete_intermediate_image_sizes( $sizes ){
	// размеры которые нужно удалить
	return array_diff( $sizes, array(
		'medium_large',
		'large',
	) );
}

?>