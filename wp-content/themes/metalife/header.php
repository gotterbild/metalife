<!doctype html>
<html <?language_attributes();?>>
<head>
	<?wp_head()?>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo('name'); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Neucha&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?bloginfo('template_directory')?>/style.css">
</head>
<body <?body_class()?> >

	<div id="mainOverlay"></div>
	<header>
		<div id="langbutton" class="hide">
			<a href="#">EN</a>
		</div>
		<div class="grid">
		<a href="/" class="mainLogoWrap headerLogo">
				<img src="<?bloginfo('template_directory')?>/images/logo-red.png" alt="<?php bloginfo('name'); ?> logo логотип" class="mainlogo">
				<img src="<?bloginfo('template_directory')?>/images/метафизикажизнинадписькрасн.png" alt="<?php bloginfo('name'); ?>" class="mainlogotext" >
			</a>
			<div id="burger">
				<span></span>
				<span></span>
				<span></span>
			</div>
			<nav id="mainMenu">
				<div class="wrap">
					<a href="/" class="mainLogoWrap">
						<img src="http://localhost:8888/metalife/wp-content/themes/metalife/images/logo-red.png" alt="Метафизика жизни logo логотип" class="mainlogo">
						<img src="http://localhost:8888/metalife/wp-content/themes/metalife/images/метафизикажизнинадписькрасн.png" alt="Метафизика жизни" class="mainlogotext">
					</a>
					<a href="#">Здоровье</a>
					<a href="#" class="active">Жизнь</a>
					<a href="#">Метафизика</a>
				</div>
			</nav>
			<div class="menu1024">
				<a href="#">
					<img class="menucell"  src="<?bloginfo('template_directory')?>/images/menuhealthgrey.png" alt="меню здоровье">
				</a>
				<a href="#" class="active">
					<img class="menucell" src="<?bloginfo('template_directory')?>/images/menulivegrey.png" alt="меню жизнь">
				</a>
				<a href="#">
					<img class="menucell" src="<?bloginfo('template_directory')?>/images/menumetgrey.png" alt="меню метафизика">
				</a>
			</div>
		</div>
	</header>