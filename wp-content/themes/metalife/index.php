<?get_header()?>

<main>
	<div class="grid">

		<?if(have_posts()):?>
			<?while(have_posts()) : the_post(); ?>

				<article>
					<a	href="<? the_permalink(); ?>">
						<img
							class="thumb"
							src="
								<?
								if( has_post_thumbnail($post->post_id) ):
									ml_post_thumb();
								else:
									echo get_template_directory_uri()."/images/no-image.png";
								endif;
								?>"
							alt="<?=$post->post_title?>"
						>
						<h2><?=$post->post_title?></h2>
					</a>
				</article>

			<? endwhile;?>
		<?php endif; ?>

		<div class="mainh1">
			<h1>Метафизика жизни - Создаём Будущее!</h1>
		</div>

	</div>
</main>

<?get_footer()?>