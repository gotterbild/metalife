window.onload = function () {

	var overlay = document.getElementById('mainOverlay');

	// Обрабатываем клик по бургеру
	var burger = document.getElementById('burger');
	var menu = document.getElementById('mainMenu');
	burger.onclick = function() {
		this.classList.toggle("close");
		menu.classList.toggle("active");
		overlay.classList.toggle("active");
	}

}