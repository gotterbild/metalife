<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'metalife');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'root');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%~^}Z8`=]2[F.17F{VL6W!8]YxDdVQ UFHD=kYDTNonrY:5V2TD)f>4%@CkY7%T#');
define('SECURE_AUTH_KEY',  'S(5!uC4Gem:I{G.,ok+Wd2$ID:@+]vlm<r?T~31O,6aFmma-I,IsALZ~EV:0r3g)');
define('LOGGED_IN_KEY',    'GOc4Ebj`Xu6iy*//IbY0Mg,IUOSGmmJb+/6Z@m4tOwLy]b<E}Z^_LcAg!e`Nzs{)');
define('NONCE_KEY',        'rx(_ 7:[!(/HGi>mNyhc&$ol YYf: P{Gz`sb^aLNAqSnMW;AfKym w)xHD5qU$i');
define('AUTH_SALT',        '{:s&:[p~e&Lz%=k|?(Ck?oO(&K >+>R4~w*?q*UXQOBQ6|5euPH3PznFmZv#CUp)');
define('SECURE_AUTH_SALT', 'jJ:7 0{XsU%Y19b?2XY2TB^qPki@VlxbpvP+9GVrtK%007daLoDMyamr;=K!koBI');
define('LOGGED_IN_SALT',   '8:~;xaK1UX+c3LIT[UHIa#(B)_(ri?I[fAYEjK<4]xsSOY)p jfuFw5UBtGaT{<R');
define('NONCE_SALT',       'vIe./RRzxdE+2eF:SNFC.EZsHXSWhVla0Vnf+,n!|,yFHX|X*UbnQFA3P$upeG[D');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
